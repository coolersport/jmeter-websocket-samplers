/*
 * FILENAME
 *     WebSocketClientStub.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2007 Genix Ventures Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     Genix Ventures ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the license agreement you entered into
 *     with Genix Ventures.
 */

package eu.luminis.websocket;

import java.io.IOException;
import java.net.URL;

import junit.framework.AssertionFailedError;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

public class WebSocketClientStub extends WebSocketClientImpl
{
    public WebSocketClientStub(URL wsURL)
    {
        super(wsURL);
    }

    @Override
    public Frame receiveFrame(int readTimeout) throws IOException {
        try {
            int waitTime = Math.min(100, readTimeout);
            if (waitTime == 0) {
                // Socket timeout of 0 means infinite
                throw new AssertionFailedError("Would block forever");
            }
            else if (waitTime < 0) {
                throw new IllegalArgumentException("timeout cannot < 0");
            }
            Thread.sleep(waitTime);
        } catch (InterruptedException e) {}

        return new BinaryFrame(new byte[0]);
    }
}
