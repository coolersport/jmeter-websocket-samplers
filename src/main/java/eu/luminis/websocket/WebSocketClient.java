/*
 * Copyright 2016, 2017 Peter Doornbosch
 *
 * This file is part of JMeter-WebSocket-Samplers, a JMeter add-on for load-testing WebSocket applications.
 *
 * JMeter-WebSocket-Samplers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * JMeter-WebSocket-Samplers is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.luminis.websocket;

import java.io.IOException;
import java.net.URL;
import java.util.Map;


public interface WebSocketClient {

    int DEFAULT_CONNECT_TIMEOUT = 20 * 1000;
    int DEFAULT_READ_TIMEOUT = 6 * 1000;

    enum WebSocketState {
        CLOSED,
        CLOSED_CLIENT,  // This side has closed
        CLOSED_SERVER,  // Other side has closed
        CONNECTED,
        CONNECTING;

        public boolean isClosing() {
            return this == CLOSED_CLIENT || this == CLOSED_SERVER;
        }
    }

    URL getConnectUrl();

    void setAdditionalUpgradeRequestHeaders(Map<String, String> additionalHeaders);

    void useProxy(String host, int port, String user, String password);

    Map<String, String>  connect() throws IOException, HttpException;

    Map<String, String> connect(int connectTimeout, int readTimeout) throws IOException, HttpException;

    Map<String, String> connect(Map<String, String> headers) throws IOException, HttpException;

    Map<String, String> connect(Map<String, String> headers, int connectTimeout, int readTimeout) throws IOException, HttpException;

    boolean isConnected();

    void dispose();

    /**
     * Close the websocket connection properly, i.e. send a close frame and wait for a close confirm.
     */
    CloseFrame close(int status, String requestData, int readTimeout) throws IOException, UnexpectedFrameException;

    void sendTextFrame(String requestData) throws IOException;

    void sendBinaryFrame(byte[] requestData) throws IOException;

    void sendPingFrame() throws IOException;

    void sendPingFrame(byte[] requestData) throws IOException;

    void sendPongFrame() throws IOException;

    void sendClose(int closeStatus, String reason) throws IOException;

    CloseFrame receiveClose(int timeout) throws IOException, UnexpectedFrameException;

    Frame receiveFrame(int readTimeout) throws IOException;

    String receiveText(int timeout) throws IOException, UnexpectedFrameException;

    byte[] receiveBinaryData(int timeout) throws IOException, UnexpectedFrameException;

    byte[] receivePong(int timeout) throws IOException, UnexpectedFrameException;
}



