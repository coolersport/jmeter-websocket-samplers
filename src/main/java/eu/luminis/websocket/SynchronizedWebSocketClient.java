/*
 * Copyright 2016, 2017 Tien Tran
 *
 * This file is part of JMeter-WebSocket-Samplers, a JMeter add-on for load-testing WebSocket applications.
 *
 * JMeter-WebSocket-Samplers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * JMeter-WebSocket-Samplers is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.luminis.websocket;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

class SynchronizedWebSocketClient implements WebSocketClient
{
    private final WebSocketClient delegate;
    
    SynchronizedWebSocketClient(URL url)
    {
        delegate = new WebSocketClientImpl(url);
    }

    @Override
    public URL getConnectUrl()
    {
        synchronized (delegate)
        {
            return delegate.getConnectUrl();
        }
    }

    @Override
    public void setAdditionalUpgradeRequestHeaders(Map<String, String> additionalHeaders)
    {
        synchronized (delegate)
        {
            delegate.setAdditionalUpgradeRequestHeaders(additionalHeaders);
        }
    }

    @Override
    public void useProxy(String host, int port, String user, String password)
    {
        synchronized (delegate)
        {
            delegate.useProxy(host, port, user, password);
        }
    }

    @Override
    public Map<String, String> connect() throws IOException, HttpException
    {
        synchronized (delegate)
        {
            return delegate.connect();
        }
    }

    @Override
    public Map<String, String> connect(int connectTimeout, int readTimeout) throws IOException, HttpException
    {
        synchronized (delegate)
        {
            return delegate.connect(connectTimeout, readTimeout);
        }
    }

    @Override
    public Map<String, String> connect(Map<String, String> headers) throws IOException, HttpException
    {
        synchronized (delegate)
        {
            return delegate.connect(headers);
        }
    }

    @Override
    public Map<String, String> connect(Map<String, String> headers, int connectTimeout, int readTimeout)
        throws IOException, HttpException
    {
        synchronized (delegate)
        {
            return delegate.connect(headers, connectTimeout, readTimeout);
        }
    }

    @Override
    public boolean isConnected()
    {
        synchronized (delegate)
        {
            return delegate.isConnected();
        }
    }

    @Override
    public void dispose()
    {
        synchronized (delegate)
        {
            delegate.dispose();
        }
    }

    @Override
    public CloseFrame close(int status, String requestData, int readTimeout)
        throws IOException, UnexpectedFrameException
    {
        synchronized (delegate)
        {
            return delegate.close(status, requestData, readTimeout);
        }
    }

    @Override
    public void sendTextFrame(String requestData) throws IOException
    {
        synchronized (delegate)
        {
            delegate.sendTextFrame(requestData);
        }
    }

    @Override
    public void sendBinaryFrame(byte[] requestData) throws IOException
    {
        synchronized (delegate)
        {
            delegate.sendBinaryFrame(requestData);
        }
    }

    @Override
    public void sendPingFrame() throws IOException
    {
        synchronized (delegate)
        {
            delegate.sendPingFrame();
        }
    }

    @Override
    public void sendPingFrame(byte[] requestData) throws IOException
    {
        synchronized (delegate)
        {
            delegate.sendPingFrame(requestData);
        }
    }

    @Override
    public void sendPongFrame() throws IOException
    {
        synchronized (delegate)
        {
            delegate.sendPongFrame();
        }
    }

    @Override
    public void sendClose(int closeStatus, String reason) throws IOException
    {
        synchronized (delegate)
        {
            delegate.sendClose(closeStatus, reason);
        }
    }

    @Override
    public CloseFrame receiveClose(int timeout) throws IOException, UnexpectedFrameException
    {
        synchronized (delegate)
        {
            return delegate.receiveClose(timeout);
        }
    }

    @Override
    public Frame receiveFrame(int readTimeout) throws IOException
    {
        synchronized (delegate)
        {
            return delegate.receiveFrame(readTimeout);
        }
    }

    @Override
    public String receiveText(int timeout) throws IOException, UnexpectedFrameException
    {
        synchronized (delegate)
        {
            return delegate.receiveText(timeout);
        }
    }

    @Override
    public byte[] receiveBinaryData(int timeout) throws IOException, UnexpectedFrameException
    {
        synchronized (delegate)
        {
            return delegate.receiveBinaryData(timeout);
        }
    }

    @Override
    public byte[] receivePong(int timeout) throws IOException, UnexpectedFrameException
    {
        synchronized (delegate)
        {
            return delegate.receivePong(timeout);
        }
    }
}
